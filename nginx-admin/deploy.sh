#!/usr/bin/env bash
#docker run -d --restart=always --net=host --name=nginx-admin \
#    -v $(pwd)/db:/app/lazy_balancer/db \
#    -v $(pwd)/nginx:/var/log/nginx \
#    v55448330/lazy-balancer:latest

if [ ! -d "./db/db.sqlite3" ];then
cp ./db/db.sqlite3.sample ./db/db.sqlite3
fi
if [ ! -d "./nginx" ];then
mkdir nginx
touch nginx/error.log
fi
docker stack deploy --compose-file=docker-stack.yml nginx