#!/usr/bin/env bash
if [ ! -d "./apisix_log" ];then
mkdir apisix_log
chmod 777 -R etcd_data
fi
if [ ! -d "./etcd_data" ];then
mkdir etcd_data
chmod 777 -R etcd_data
fi
docker stack deploy --compose-file=docker-stack.yml nginx
