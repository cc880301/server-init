#!/usr/bin/env bash
if [ ! -d "./code" ];then
mkdir code
fi
docker stack deploy --compose-file=docker-stack.yml $(basename `pwd`)