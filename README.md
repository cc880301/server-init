

## 安装
```
sh start.sh
```
如果swarm没初始化过，先执行以下命令
```
docker swarm init
```

## 使用
### portainer
See https://github.com/portainer/portainer-compose

  ```
  http://127.0.0.1:9000/
```


### nginx-admin
See https://gitee.com/v55448330/lazy-balancer
  ```
http://127.0.0.1:8000/
admin
123456789
```
### git-hooks
See https://gitee.com/cc880301/git

### 示例 
See ./demo \
注意：需要连接 nginx_proxy 网络才可被 nginx-admin 负载发现